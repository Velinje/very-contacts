const path = require('path');
const webpack = require('webpack');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');


module.exports = {
  context: path.resolve(__dirname, 'src'),
  entry: ['babel-polyfill','./index.js'],
  output: {
    path: path.resolve(__dirname, 'app'),
    filename: '[name].js',
    sourceMapFilename: '[name].map'
  },
  module: {
    rules: [{
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        loader: "babel-loader"
      },
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [{
            loader: "css-loader"
          }, {
            loader: "sass-loader"
          }]
        })
      }
    ]
  },
  plugins: [
    new webpack.LoaderOptionsPlugin({
      minimize: true,
      debug: false
    }),
    new ExtractTextPlugin("style.css"),
  ],
  resolve: {
    extensions: ["*", ".js", ".json", ".jsx", ".css", ".scss"],
  },
  devServer: {
    contentBase: path.join(__dirname, "app"),
    compress: true,
    port: 9000,
    historyApiFallback: true
  },
}