//Contact structure, defining state for empty data
export default {
            'gender': '',
			'name': {
				'title': '',
				'first': '',
				'last': ''
			},
			'location': {
				'street': '',
				'city': '',
				'state': '',
				'postcode': ''
			},
			'email': '',
			'login': {
				'username': '',
				'password': '',
				'salt': '',
				'md5': '',
				'sha1': '',
				'sha256': ''
			},
			'dob': '',
			'registered': '',
			'phone': '',
			'cell': '',
			'id': {
				'name': '',
				'value': ''
			},
			'picture': {
				'large': ''
			},
			'nat': ''
}