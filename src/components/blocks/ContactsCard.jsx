import React from 'react';
import {Link, Route} from 'react-router-dom';

const ContactsCard = ({contact}) =>
        <article className="contacts-card mod_scale_fluid">
            <img src={contact.picture.large} title={`${contact.name.first} ${contact.name.last}`}/>
            <div className={'contacts-card__info'}>
                    <p><span>Gender:</span> <span>{contact.gender}</span></p>
                    <p><span>Name:</span> <span>{contact.name.first}</span> <span>{contact.name.last}</span></p>
                    <p><span>Location:</span> <span>{contact.location.city}</span></p>
                    <p className={'email'}> <span>Email:</span> <a href={`mailto:${contact.email}`}>{contact.email}</a></p>
                    <Link className="contacts-card__details" to={`/contact/${contact.id.value ? contact.id.value : 'noid'}-${contact.name.first}${contact.name.last}`}>Details</Link>
            </div>
        </article>;

export default ContactsCard;