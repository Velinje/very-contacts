import React from 'react';

export default ({loading = false}) => 
    <div className={`connecting ${loading ? 'connecting--animate' : ''}`}></div>;