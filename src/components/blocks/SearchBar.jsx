import React from 'react';

const SearchBar = ({abc, directionReverse, onChange}) => {

function setValue(newValue){
    onChange(newValue.currentTarget.value);
}

 return <div className="searchbar__container">
            <div className="searchbar__block">
                <div className="searchbar__panel">
                    <input onChange={value => setValue(value)} placeholder="search by name" type="search" name="search"/>
                    <button className={directionReverse ? 'searchbar__abc searchbar__abc--reverse': 'searchbar__abc'} onClick={ abc }>ABC</button>
                </div>
            </div>
        </div>
}
    
export default SearchBar;