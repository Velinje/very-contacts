import React from 'react';
import {connect} from 'react-redux';
import ContactsCard from '../blocks/ContactsCard';
import SearchBar from '../blocks/SearchBar';
import Connecting from '../blocks/Connecting';
import {getContacts, searchContacts, sortByABC} from '../../actions/data';

class Home extends React.Component {

constructor(props) {
  super(props);
  this.state = {
    reverseSort: true,
    sortedList: [],
  }
}

   componentDidMount() {
       this.props.dispatch(getContacts());
  }

  search = value => {
    this.props.dispatch(searchContacts(value));
  }

  abcSort = () => {
    this.setState({reverseSort: !this.state.reverseSort})
    this.props.dispatch(sortByABC(this.state.reverseSort));
  }

    render() {
        return (
            <main>
             <h1>Contacts</h1>
                <Connecting loading={this.props.contacts.loading}/>
                {this.props.contacts.contacts.map(contact => <ContactsCard key={contact.id.value + contact.name.first} contact={contact} />)}
                <SearchBar onChange={this.search} abc={this.abcSort} directionReverse={this.state.reverseSort} disabled={`${this.props.contacts.contacts.length ? '' : 'disabled'}`}/>
            </main>
        )}
    };

function mapStateToProps({contacts, loading}, ownProps){
    return {
        contacts, 
        loading
    };
}

export default connect(mapStateToProps)(Home);
