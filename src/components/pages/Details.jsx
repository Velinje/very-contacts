import React from 'react';
import Connecting from '../blocks/Connecting';
import { getContact } from '../../actions/data'
import {connect} from 'react-redux';

class Details extends React.Component {

   componentDidMount() {
       this.props.dispatch(getContact(this.props.match.params.contactsId.split('-')[0]));
  }

    render() {
        console.log(this.props);
        const details = this.props.details;
        return (
            <main>
            <h1>Details</h1>
             <Connecting loading={this.props.loading}/>
                <div className="contact__details mod_scale_fluid">
                    <img src={details.picture.large}/>
                    <div className="contact__name">
                        <h2>{`${details.name.first} ${details.name.last}`}</h2>
                    </div>
                        <div>
                            <h4>Bio</h4>
                            <ul>
                                <li>gender: {details.gender}</li>
                            </ul>
                        </div>
                        <div>
                            <h4>Contact info</h4>
                            <ul>
                             <li><a href={`mailto:${details.email}`}> email: {details.email}</a></li>
                             <li><a href={`tel:${details.phone}`}>phone: {details.phone}</a></li>
                             <li><a href={`tel:${details.cell}`}>cell: {details.cell}</a></li>
                             </ul>
                        </div>
                        <div><h4>Location</h4>
                            <ul className="contact__details__location">
                            {Object.keys(details.location).map( loc => details.location[loc] && <li key={loc}>{  `${loc}: ${details.location[loc]}`}</li>)}
                            </ul>
                        </div>
                </div>
            </main>
        )
    }
};

function mapStateToProps ({contacts, loading}, ownProps){
    
    return {
        details: contacts.details,
        loading
    };
}

export default connect(mapStateToProps)(Details);