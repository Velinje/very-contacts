
/**
 * Description [Simple points based search algorithm using first and last name]
 * @params { first, last, comparer } takes first and last name and gives matching placement and charact points
 * @return { Int } Int value of points
 */
function compareName(first, last, comparer){
    
    const strLength = comparer.length;
    let points = 0;

    for (var index = 0; index < strLength; index++) {
       if(first[index].toLowerCase() === comparer[index].toLowerCase()){
           points = points + 1;
       };

       if(!first[index + 1]){ break;}
    }

     for (var index = 0; index < strLength; index++) {
       if(last[index].toLowerCase() === comparer[index].toLowerCase()){
           points = points + 1;
       };

       if(!last[index + 1]){ break;}
    }

    return points;
}

/**
 * Description [Search function using first and last name]
 * @params { searchString, contacts } string for comparison, property to compare with
 * @return { Array } Array of contacts
 */
export function byName(searchString, contacts) {
    if(searchString.length === 0){
        return alphabetically(contacts, false);
    }
    return contacts.sort((a, b) => { return compareName(b.name.first, b.name.last, searchString) - compareName(a.name.first, a.name.last, searchString); });
};


/**
 * Description [Sort function using first and last name, sorts alphabetically]
 * @params { contacts, reverse } array of contacts, and reverse defines direction of the return content
 * @return { Array } Array of contacts
 */
export function alphabetically(contacts, reverse) {
    const sortedArray = contacts.sort((a, b) => { 
        if(`${a.name.first}  ${a.name.last}` < `${b.name.first}  ${b.name.last}`) return -1; 
        if(`${a.name.first}  ${a.name.last}` > `${b.name.first}  ${b.name.last}`) return 1; 
        return 0;
    });
    return reverse ? sortedArray.reverse() : sortedArray;
}