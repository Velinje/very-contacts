import React from 'react'
import Home from './components/pages/home';
import Details from './components/pages/details';
import { HashRouter as Router, Route, Link } from 'react-router-dom';
import configureStore from './store/configureStore';
import {Provider} from 'react-redux';


export const store = configureStore();

const App = () => (
      <Provider store={store}>
        <Router hashType="noslash">
          <div>
            <Route path="/contact/:contactsId" component={Details} />
            <Route exact path="/" component={Home}/>
          </div>
        </Router>
      </Provider>
)

export default App;