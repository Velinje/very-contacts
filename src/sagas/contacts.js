import { put, call } from 'redux-saga/effects';
import { fetchAllContacts, fetchContact } from '../api/data';
import * as types from '../constants/actionTypes';

/**
 * Saga generators for PullAllContacts and for Contact
 */
export function* pullAllContacts() {
  try {
    const contacts = yield call(fetchAllContacts);
    yield [
      put({ type: types.CONTACTS_FETCH_SUCCEEDED, contacts })
    ];
  } catch (error) {
    yield put({ type: types.CONTACTS_FETCH_FAILED, error });
  }
}

export function* pullContact({id}) {
  try {
    const contact = yield call(fetchContact,id);
    yield [
      put({ type: types.CONTACT_FETCH_SUCCEEDED, contact})
    ]
  } catch (error) {
    yield put({type: types.CONTACT_FETCH_FAILED});
  }
}