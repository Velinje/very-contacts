import { fork } from 'redux-saga/effects';
import { watchFetchContacts } from './watchers';

/**
 * Initialize watchers
 */
export default function* contactsInitializer() {
  yield fork(watchFetchContacts);
}