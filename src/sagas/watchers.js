import { takeLatest } from 'redux-saga/effects';
import { pullAllContacts, pullContact } from './contacts';
import * as types from '../constants/actionTypes';

/**
 * Setup Watchers for REQUEST actions
 */
export function* watchFetchContacts() {
  yield takeLatest(types.CONTACTS_FETCH_REQUESTED, pullAllContacts);
  yield takeLatest(types.CONTACT_FETCH_REQUESTED, pullContact);
};