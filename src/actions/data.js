import * as types from '../constants/actionTypes';

export const getContacts = () => ({
  type: types.CONTACTS_FETCH_REQUESTED
});

export const getContact = (id) => ({
  type: types.CONTACT_FETCH_REQUESTED,
    id
});

export const loading = () => ({
  type: types.CONTACTS_LOADING
});

export const sortByABC = (reverseSort) => ({
  type: types.CONTACTS_SORT_ABC,
  reverseSort
});

export const searchContacts = (searchString) => ({
  type: types.CONTACTS_SEARCH_REQUESTED,
    searchString
});