import {byName, alphabetically} from '../utils/sort';
import {loading} from '../actions/data';
import {store} from '../App';

/**
  * Description [fetch Url base]
 */
const urlBase = 'https://randomuser.me/api/';

/**
 * Description [Get all contacts]
 * @return { Array } Array of contacts
 */
export function fetchAllContacts() {
    const state = store.getState();

    if(state.contacts.contacts.length > 0){
        return;
    }

    store.dispatch(loading());
    return fetch(`${urlBase}?seed=troll&results=100&inc=name,gender,name,location,email,id,picture`)
        .then(response => {
            return response.json();
        })
        .then((jsonData) => {
            return jsonData;
        })
}

/**
 * Description [Search contacts]
 * @params { searchString } string for comparison, property to compare with
 * @return { Array } Array of contacts
 */
export function search({searchString}) {
    return byName(searchString, store.getState().contacts.contacts);
}

/**
 * Description [Get contact by id]
 * @params { id } identity variable
 * @return { Object } Contact
 */
export function fetchContact(id) {
    return fetch(`${urlBase}?results=1&id=${id}`)
        .then(response => {
            return response.json();
        })
        .then((jsonData) => {
            return jsonData.results[0];
        })
}