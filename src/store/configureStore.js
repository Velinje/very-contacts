import {createStore, applyMiddleware } from 'redux';
import rootReducer from '../reducers';
import createSagaMiddleware from 'redux-saga';
import rootSaga from '../sagas';

export default (initialState) => {
    const sagaMiddleware = createSagaMiddleware();

    return { ...createStore(rootReducer, initialState,
    applyMiddleware(sagaMiddleware)),
        runSaga: sagaMiddleware.run(rootSaga)
    }
}