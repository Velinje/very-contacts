import * as types from '../constants/actionTypes';
import contact from '../constants/contact';
import {search} from '../api/data';
import { alphabetically } from '../utils/sort';

export default (state = {contacts: [], details: contact ,loading: false, reverseSort: true}, action) => {
    switch (action.type) {
        case types.CONTACTS_LOADING:
            return {...state, loading: true};
            break;
        case types.CONTACTS_FETCH_SUCCEEDED:
            return {...state, contacts: alphabetically(action.contacts.results, action.reverseSort), loading: false};
            break;
        case types.CONTACT_FETCH_REQUESTED:
            return {...state,loading:true};
            break;
        case types.CONTACT_FETCH_SUCCEEDED:
            return {...state, details: action.contact, loading:false};
            break;
        case types.CONTACTS_SEARCH_REQUESTED:
            return {...state, contacts: search(action)};
            break;
        case types.CONTACTS_SORT_ABC:
            return {...state, contacts: alphabetically(state.contacts, action.reverseSort)}
        default:
            break;
    }
    
    return state;
}