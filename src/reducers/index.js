import {combineReducers} from 'redux';
import contacts from './contacts';

/**
 * Combine reducers, prepared for expansion
 */
const rootReducer = combineReducers({
    contacts
});

export default rootReducer;